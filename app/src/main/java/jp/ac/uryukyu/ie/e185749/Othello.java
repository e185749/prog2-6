package jp.ac.uryukyu.ie.e185749;
import java.util.Scanner;

public class Othello {
    
    public static void main(String[] args){
        
        Scanner sc =new Scanner(System.in);

        System.out.println("プレイヤー1の名前 : ");
        String name1 = sc.nextLine();
        System.out.println("プレイヤー1のコマ名 : ");
        char sign1 = sc.nextLine().charAt(0);

        System.out.println("プレイヤー2の名前 : ");
        String name2 = sc.nextLine();
        System.out.println("プレイヤー2のコマ名 : ");
        char sign2 = sc.nextLine().charAt(0);

        User user1 = new User(name1,sign1);
        User user2 = new User(name2,sign2);

        Board board = new Board(sign1,sign2);

        System.out.println("\n決闘開始!!\n");

        boolean player = true;
        board.printBoard();

        while(true){
            if(player){
                System.out.println("\n"+ user1.getName()+" あなたのコマ名は "+user1.getColor());
                System.out.println("縦軸を入力してください。[0-7] "+user1.getName()+" : ");
                int x = 0;
                int y = 0;

                try{
                    x = sc.nextInt();
                }catch(Exception e){
                    System.out.println("そこにはおけません！再入力してください。");
                    continue;
                }
                finally{
                    sc.nextLine();
                }

                System.out.println("横軸を入力してください。[0-7] "+user1.getName()+" : ");

                try{
                    y = sc.nextInt();
                }catch(Exception e){
                    System.out.println("そこにはおけません！再入力してください。");
                    continue;
                }
                finally{
                    sc.nextLine();
                }

                int curr = board.move(user1,user2,x,y);
                if(curr==-1){
                    System.out.println("パスしますか。[y/n]: ");
                    char pass = sc.next().charAt(0);
                    if(pass=='Y' || pass=='y')
                        player = false;
                    continue;
                }
                
                if(curr==2)
                    break;
                
                player = false;
            }
            else{
                System.out.println("\n"+ user2.getName()+" あなたのコマ名は "+user2.getColor());
                System.out.println("縦軸を入力してください。[0-7]"+user2.getName()+" : ");
                int x = 0;
                int y = 0;
                try{
                    x = sc.nextInt();
                }catch(Exception e){
                    System.out.println("そこにはおけません！再入力してください。");
                    continue;
                }
                finally{
                    sc.nextLine();
                }
                System.out.println("横軸を入力してください。[0-7] "+user2.getName()+" : ");
                try{
                    y = sc.nextInt();
                }catch(Exception e){
                    System.out.println("そこにはおけません！再入力してください。");
                    continue;
                }
                finally{
                    sc.nextLine();
                }
                int curr = board.move(user2,user1,x,y);
                if(curr==-1){
                    System.out.println("パスしますか。[y/n]: ");
                    char pass = sc.next().charAt(0);
                    if(pass=='Y' || pass=='y')
                        player = false;
                    continue;
                }
                if(curr==2)
                    break;
                
                player = true;
            }
        }

    }
}
